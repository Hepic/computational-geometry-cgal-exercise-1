#include <iostream>
#include <iterator>
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/convex_hull_2.h>

using namespace std;

typedef CGAL::Exact_predicates_inexact_constructions_kernel Kernel;
typedef Kernel::Point_2 Point2;


int main() {
    istream_iterator<Point2> inputBegin(cin);
    istream_iterator<Point2> inputEnd;
    ostream_iterator<Point2> output(cout, "\n");
    
    CGAL::convex_hull_2(inputBegin, inputEnd, output);
    return 0;
}
