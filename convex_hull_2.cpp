#include <iostream>
#include <fstream>
#include <vector>
#include <CGAL/Exact_predicates_exact_constructions_kernel.h>
#include <CGAL/convex_hull_2.h>
#include <CGAL/intersections.h>

using namespace std;

typedef CGAL::Exact_predicates_exact_constructions_kernel Kernel;
typedef Kernel::Point_2 Point2;
typedef Kernel::Segment_2 Segment2;
typedef Kernel::Line_2 Line2;
typedef Kernel::Intersect_2 Intersect2;


int main(int argc, char *argv[]) {
    if (argc != 3) {
        printf("Error with arguments\n");
        return 0;
    }

    ifstream input(argv[1]);
    ofstream output(argv[2]);
    vector<Point2> points, resultConvex;
    Line2 line;
    double X, Y;

    if (input.is_open()) {
        while (input >> X >> Y) {     
            points.push_back(Point2(X, Y));
        }
        
        Point2 pnt1 = points.back();
        points.pop_back();
        
        Point2 pnt2 = points.back();
        points.pop_back();
        
        // create a line with the last two points of the file
        line = Line2(pnt1, pnt2);
    }

    if (output.is_open()) {
        CGAL::convex_hull_2(points.begin(), points.end(), back_inserter(resultConvex));

        for (int i = 0; i < resultConvex.size(); ++i) {
            output << resultConvex[i] << endl; // write convex hull in the output file

            // for every possible segment of convex hull, check for intersection
            Segment2 polygonSegment(resultConvex[i], resultConvex[(i + 1) % resultConvex.size()]);
            
            CGAL::cpp11::result_of<Intersect2(Line2, Segment2)>::type
            interRes = intersection(line, polygonSegment);
            
            if (interRes) {
                if (const Segment2 *segInter = boost::get<Segment2>(&*interRes)) {
                  cout << "Segment: " << *segInter << endl;
                } else {
                  const Point2 *pntInter = boost::get<Point2>(&*interRes);
                  cout << "Point: " << *pntInter << endl;
                }
            }
        }
    }
    
    input.close();
    output.close();
    return 0;
}
