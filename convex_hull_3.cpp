#include <iostream>
#include <fstream>
#include <vector>
#include <CGAL/Exact_predicates_exact_constructions_kernel.h>
#include <CGAL/convex_hull_2.h>
#include <CGAL/point_generators_3.h>
#include <CGAL/Projection_traits_yz_3.h>
#include <ctime>

using namespace std;

typedef CGAL::Exact_predicates_exact_constructions_kernel Kernel;
typedef Kernel::Point_3 Point3;
typedef CGAL::Projection_traits_yz_3<Kernel> trait;


int main(int argc, char *argv[]) {
    if (argc != 2) {
        printf("Error with arguments\n");
        return 0;
    }

    int N;
    double R;
    vector<Point3> points, resultConvex;
    ofstream output("output.txt");
    
    if (scanf("%d%lf", &N, &R) != 2) {
        return 0;
    }
   
    // get N random points
    CGAL::Random_points_on_sphere_3<Point3> generator(R);
    CGAL::cpp11::copy_n(generator, N, std::back_inserter(points));

    clock_t begin = clock();
    CGAL::ch_akl_toussaint(points.begin(), points.end(), back_inserter(resultConvex), trait());
    clock_t end = clock();
    double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
    
    cout << elapsed_secs << " secs passed" << endl;

    if (output.is_open()) {
        for (int i = 0; i < resultConvex.size(); ++i) {
            output << resultConvex[i] << endl;
        }
    }
    
    output.close();
    return 0;
}
